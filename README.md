# Payton's README

This README is intended to help my colleagues know a little bit more about me and how I work. This is not meant to be a static file, and its contents will change as I learn how to navigate through life at GitLab.

* [🦊 My GitLab handle](https://gitlab.com/pburdette)
* [🏀 Team page](https://about.gitlab.com/company/team/#pburdette)

## Who I am

* My name is Payton Burdette and I'm a Senior Frontend Engineer for Pipeline Execution.
* I was born and raised in Anderson, SC USA (where I currently still reside).
* I have a lovely wife Ashely, a beautiful daughter Lynley and two goldendoodles Macy/Maggie.
* I enjoy working out, playing golf, fishing and watching Anime.
* I love helping others, If I can help out please let me know.

## How I work

* I typically start my mornings going through pings and todos, then move onto my issues for the milestone.
* I move fast and I'm a big believer in iteration.
* I perfer async communication over everything, I'm not a big fan of sync meetings (this stems from my struggle with anxiety).
* Teamwork makes the dreamwork, we need each other to succeed.

## Availability

* My timezone is [EST](https://time.is/New_York).
* I generally work from 8:00am to 4:00pm.
     * I sometimes come back to my desk late at night after I get my daughter down for bed for some quick work. 30 minutes to an hour at most.
     * I usually do not accept meetings late-nights or early mornings. 
* **How to reach me:**
     * **Slack**: DM or mention is the best way to reach me during the day. 
     * **GitLab**: Please @ me directly in comments on GitLab so I see your message, and assign MRs to me. 

## Communication style

* My preferred method of communication is GitLab and Slack. I'm not a fan of randmom meetings to discuss something that could of been done async. We should respect each others time.
* If I ever message you please don't feel obligated to respond immediately, I understand it may take some time to get back to me.
